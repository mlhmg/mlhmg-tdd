// $(function(){
//     $('#searchForm').submit(function(event){
//         let searchName = $('#booksName').val();
//         let url = "https://www.googleapis.com/books/v1/volumes?q=" + searchName;

//         $.ajax({
//             url: url,
//             dataType: 'json',
//             succes: function(searchRes){
//                 let bookShelf = searchRes.items;


//                 for (i = 0; i < count; i++) {
//                     let book = bookShelf[i].volumeInfo;
//                     result.innerHTML += "<h2>" + book.title + "</h2>"

//                 }
//             },
//             error : function(){
//                 alert("monmaap")
//             },
//             type: 'GET',

//         });
//         event.preventDefault();
//     })
// })
$(document).on({
    ajaxStart: function () {
        $('.none').css('display', 'block');
    },
    ajaxStop: function () {
        $('.none').css('display', 'none');
    }
});


$(document).ready(function () { 
    $('.form-inline').submit(function (event) {
        let searchItem = $('#input').val();
        let urlItem = "https://www.googleapis.com/books/v1/volumes?q=" + searchItem;

        $.ajax({
            async: true,
            type: 'GET',
            url: urlItem,
            dataType: 'json',
            success: function (searchRes) {
                let bookShelf = searchRes.items;

                $('tbody').empty();

                for (i = 0; i < 20; i++) {
                    let book = bookShelf[i].volumeInfo;
                    var name = $('<td>').text(book.title);

                    if ('authors' in book == false) var authors = $('<td>').text("-");
                    else var authors = $('<td>').text(book.authors);

                    if ('categories' in book == false) var categories = $('<td>').text("-");
                    else var categories = $('<td>').text(book.categories);

                    if ('imageLinks' in book == false)
                        var img = $('<td>').text("-");
                    else {
                        if ('smallThumbnail' in book.imageLinks == false)
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }

                    var tr = $('<tr>').append( name, authors, categories, img);

                    $('tbody').append(tr);

                }
            },
            error: function () {
                $('.status-container').next().addClass('none');
                alert("Mohon maaf terjadi kesalahan. Coba ulangi lagi ya!");
            },
            type: 'GET',
        })
        event.preventDefault();
    })

});
