$(function(){
    $("dt").click(function(){
        $(this).toggleClass("open");
        if($(this).hasClass("open")){
            $("dt").not(this).removeClass("open");
        }
    })
})

const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');

function switchTheme(e) {
    if (e.target.checked) {
        document.documentElement.setAttribute('data-theme', 'dark');
    }
    else {
        document.documentElement.setAttribute('data-theme', 'light');
    }    
}

toggleSwitch.addEventListener('change', switchTheme, false);