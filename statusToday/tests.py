from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.http import HttpRequest
from datetime import datetime

from .models import statusModel
from .views import status
from .forms import statusForm

from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.keys import Keys
import time


class FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('headless')
        self.options.add_argument('no-sandbox')
        self.options.add_argument('disable-dev-shm-usage')
        self.options.add_argument('disable-extensions')
        self.browser = webdriver.Chrome(options=self.options, executable_path='./chromedriver')
        # self.browser = webdriver.Chrome('statusToday/chromedriver.exe')

    def test_homepage_text_is_displayed(self):
        self.browser.get(self.live_server_url)

        self.assertEquals(self.browser.find_element_by_tag_name('h1').text, 'HOMEPAGE')
    
    def test_status_button_redirect_to_status_page(self):
        self.browser.get(self.live_server_url)

        status_url = self.live_server_url + reverse('status')
        self.browser.find_element_by_id('status_butt').click()
        self.assertEquals(
            self.browser.current_url,
            status_url
        )
    def test_if_status_are_posted(self):
        self.browser.get(self.live_server_url)

        self.browser.find_element_by_id('status_butt').click()

        time.sleep(3)
        new_status = self.browser.find_element_by_id('id_status')
        new_status.send_keys("testing input")
        time.sleep(3)

        submit = self.browser.find_element_by_id('tombol')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)

        # self.browser.get(self.live_server_url)
        time.sleep(3)
        self.assertIn("testing input", self.browser.page_source)



    def tearDown(self):
        self.browser.close()
    



        

# Create your tests here.
class PepewStatusUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_status_page_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/somethingWrong/')
        self.assertEqual(response.status_code, 404)

     # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_status_page_using_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_status_page_uses_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'status.html')

        # - - - - - - - - - MODEL TEST - - - - - - - - 
    def create_status(self, 
            status       = 'main dota seru',
            time         = datetime.now(),
        ):

        return statusModel.objects.create(
            status=status,
            time=time,
        )

    def test_status_creation(self):
        obj = self.create_status()
        self.assertTrue(isinstance(obj, statusModel))
        self.assertEqual(obj.__str__(), obj.status)

         # - - - - - - - - - FORM TEST - - - - - - - - -  -
    def test_valid_form(self):
        obj = statusModel.objects.create(
            status       = 'main dota seru',
            time         = timezone.now(),
        )

        data = {
            'status' : obj.status,
            'time'       : obj.time,
        }

        form = statusForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        obj = statusModel.objects.create(
            status       = '',
            time         = timezone.now(),
        )

        data = {
            'status'     : obj.status,
            'time'       : obj.time,
        }

        form = statusForm(data=data)
        self.assertFalse(form.is_valid())
