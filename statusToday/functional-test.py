from selenium import webdriver
import unittest
import time
class NewTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def test_can_start(self):
        self.browser.get('https://google.com')
        time.sleep(1)
        search_box = self.browser.find_element_by_name('q')
        search_box.send_keys('TDD skkuy')
        search_box.submit()
        time.sleep(1)
        self.assertIn("TDD", self.browser.page_source)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
