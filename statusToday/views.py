from django.shortcuts import render, redirect

from .models import statusModel
from .forms import statusForm

def status(request):
    status_form = statusForm(request.POST or None)
    all_status = statusModel.objects.all()

    if request.method == 'POST':
        if status_form.is_valid():
            status_form.save()
            return redirect('status')

    context = {
        'status_form' : status_form,
        'all_status' : all_status
    }

    return render(request,'status.html',context)


def homepage(request):
    return render(request, 'homepage.html')


# Create your views here.
