from django import forms
from .models import statusModel

class statusForm(forms.ModelForm):
    class Meta:
        model = statusModel
        fields = ['status']
        widget = {
            'status' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'What happen today?'
                }
            )
        }