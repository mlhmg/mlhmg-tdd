from django.db import models
from datetime import datetime

# Create your models here.

class statusModel(models.Model):
        status      = models.CharField(max_length = 300)
        time        = models.DateTimeField(default = datetime.now, blank = True)

        def __str__(self):
            return self.status