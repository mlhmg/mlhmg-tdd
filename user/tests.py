from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.http import HttpRequest
from datetime import datetime

from django.contrib import admin
from django.contrib.auth.models import User

# from djangp.contrib.auth import views as auth_views
from . import views
# Create your tests here.
class LoginUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_login_page_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/ablebleeehhh/')
        self.assertEqual(response.status_code, 404)

     # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_login_page_using_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, views.login_view)

    def test_login_page_uses_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
    def test_login(self):
        # send login data
        response = self.client.post('/login/', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)

class LogoutUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    # def test_logout_page_is_exist(self):
    #     response = Client().get('/logout/')
    #     self.assertEqual(response.status_code,302)

    # def test_invalid_url_not_found(self) :
    #     response = Client().get('/ablebleeehhh/')
    #     self.assertEqual(response.status_code, 404)

     # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_logout_page_using_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, views.logout_view)

    # def test_logout_page_uses_template(self):
    #     response = Client().get('/logout/')
    #     self.assertTemplateUsed(response, 'logout.html')


# Create your tests here.
