from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.http import HttpRequest
from datetime import datetime

from .views import about

# Create your tests here.
class AboutAppUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_about_page_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/ablebleeehhh/')
        self.assertEqual(response.status_code, 404)

     # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_status_page_using_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_status_page_uses_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')