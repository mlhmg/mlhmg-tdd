from django.urls import path,include
from . import views


urlpatterns = [
    path('bookSearch/', views.bookSearch, name='bookSearch'),
]