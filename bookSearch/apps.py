from django.apps import AppConfig


class BooksearchConfig(AppConfig):
    name = 'bookSearch'
